# ScalarStateRWExample

Tango device class with a scalar RW attribute.

If you're not working at the ESRF, please re-generate the Makefile or CMakeLists.txt with POGO or adapt the provided Makefile to your current installation.

Please note that to compile with cppTango main branch, you'll need to use the `-std=c++14` compiler option.

